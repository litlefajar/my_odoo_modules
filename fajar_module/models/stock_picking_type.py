# -*- coding: utf-8 -*-

from ast import literal_eval
from datetime import date
from itertools import groupby
from operator import itemgetter
import time

from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.osv import expression
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.float_utils import float_compare, float_is_zero, float_round
from odoo.exceptions import UserError
from odoo.addons.stock.models.stock_move import PROCUREMENT_PRIORITIES

class StockPickingType(models.Model):
    _inherit = 'stock.picking.type'

    def name_get(self):
        """ Display 'Warehouse_name: PickingType_name' """
        res = []
        for picking_type in self:
            picking_type.check_trans(picking_type.id)
            if picking_type.warehouse_id:
                name = picking_type.warehouse_id.name + ': ' + picking_type.name
            else:
                name = picking_type.name
            res.append((picking_type.id, name))
        return res

    def check_trans(self, picking_type_id):
        list_lang = self.env['res.lang'].search([('active','=',True)])
        stock_picking = self.env['stock.picking.type'].search([('id','=',picking_type_id)])
        source_picking = self.env['stock.picking.type'].search(['&',('code','=',stock_picking.code),('company_id','=',1)])
        for lang in list_lang:
            look_val = self.env['ir.translation'].search(['&','&',('name','=','stock.picking.type,name'),('res_id','=',source_picking.id),('lang','=',lang.code)])
            if(look_val.value==''):
                self.env['ir.translation']._upsert_translations([{
                    'type': 'model',
                    'name': 'stock.picking.type,name',
                    'lang': lang.code,
                    'res_id': picking_type_id,
                    'src': stock_picking.code,
                    'value': look_val.src,
                    'state': 'translated',
                }])
            else:
                self.env['ir.translation']._upsert_translations([{
                    'type': 'model',
                    'name': 'stock.picking.type,name',
                    'lang': lang.code,
                    'res_id': picking_type_id,
                    'src': stock_picking.code,
                    'value': look_val.value,
                    'state': 'translated',
                }])
            # trans=self.env['ir.translation'].search(['&','&',('name','=','stock.picking.type,name'),('res_id','=',picking_type_id),('lang','=',lang.code)])
            # if(trans)


